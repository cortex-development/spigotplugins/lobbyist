package me.kodysimpson.lobbyist.commands;

import me.kodysimpson.lobbyist.Lobbyist;
import me.kodysimpson.lobbyist.utils.ColorUtils;
import me.kodysimpson.lobbyist.utils.LobbySpawnUtil;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetLobbyCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {


        if (sender instanceof Player){

            Player p = (Player) sender;

            if (p.hasPermission("lobbyist.setlobby") || p.hasPermission("lobbyist.admin")){
                LobbySpawnUtil.setLobbySpawn(p.getLocation());

                p.sendMessage(ChatColor.GREEN + "You have set the lobby location.");
            }else{
                p.sendMessage(ColorUtils.translateColorCodes(Lobbyist.getPlugin().getConfig().getString("messages.no-permission.command")));
            }

        }


        return true;
    }

}
