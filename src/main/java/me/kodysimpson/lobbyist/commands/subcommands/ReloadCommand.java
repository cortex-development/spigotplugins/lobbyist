package me.kodysimpson.lobbyist.commands.subcommands;

import me.kodysimpson.lobbyist.Lobbyist;
import me.kodysimpson.lobbyist.commands.SubCommand;
import me.kodysimpson.lobbyist.utils.ColorUtils;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class ReloadCommand extends SubCommand {

    @Override
    public String getName() {
        return "reload";
    }

    @Override
    public String getDescription() {
        return "Reload the configuration for Lobbyist";
    }

    @Override
    public String getSyntax() {
        return "/lobbyist reload";
    }

    @Override
    public void perform(Player p, String[] args) {

        if (p.hasPermission("lobbyist.reload") || p.hasPermission("lobbyist.admin")) {

            Lobbyist.getPlugin().reloadConfig();

            p.sendMessage(ChatColor.GREEN + "The config.yml has been successfully reloaded.");

        } else {
            p.sendMessage(ColorUtils.translateColorCodes(Lobbyist.getPlugin().getConfig().getString("messages.no-permission.command")));
        }

    }

    @Override
    public List<String> tabComplete(Player player, String[] args) {
        return null;
    }
}
