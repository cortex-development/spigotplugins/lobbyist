package me.kodysimpson.lobbyist.commands.subcommands;

import me.kodysimpson.lobbyist.commands.CommandManager;
import me.kodysimpson.lobbyist.commands.SubCommand;
import me.kodysimpson.lobbyist.utils.ColorUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.List;

public class HelpCommand extends SubCommand {
    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "See all of the commands for Lobbyist";
    }

    @Override
    public String getSyntax() {
        return "/lobbyist help";
    }

    @Override
    public void perform(Player p, String[] args) {

        CommandManager commandManager = new CommandManager();

        p.sendMessage(" ");
        p.sendMessage(ColorUtils.translateColorCodes("&7&m---------------&a&l[&#ff29ad&lLobbyist&a&l]&7&m---------------"));
        p.sendMessage(" ");
        for (int i = 0; i < commandManager.getSubCommands().size(); i++) {
            p.sendMessage(ChatColor.DARK_GRAY + " - " + ChatColor.YELLOW + commandManager.getSubCommands().get(i).getSyntax() + " - " + ChatColor.GRAY + commandManager.getSubCommands().get(i).getDescription());
        }
        p.sendMessage(ChatColor.DARK_GRAY + " - " + ChatColor.YELLOW + "/setlobby - " + ChatColor.GRAY + "Set the lobby spawn");
        p.sendMessage(ChatColor.DARK_GRAY + " - " + ChatColor.YELLOW + "/lobby - " + ChatColor.GRAY + "Teleport to the lobby spawn");
        p.sendMessage(" ");
        p.sendMessage(ColorUtils.translateColorCodes("&7&m----------------------------------------"));
        p.sendMessage(" ");


    }

    @Override
    public List<String> tabComplete(Player player, String[] args) {
        return null;
    }
}
