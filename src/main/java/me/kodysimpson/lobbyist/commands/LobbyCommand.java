package me.kodysimpson.lobbyist.commands;

import me.kodysimpson.lobbyist.Lobbyist;
import me.kodysimpson.lobbyist.utils.ColorUtils;
import me.kodysimpson.lobbyist.utils.LobbySpawnUtil;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LobbyCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player){

            Player p = (Player) sender;

            if (p.hasPermission("lobbyist.lobby") || p.hasPermission("lobbyist.admin")){
                Location location = LobbySpawnUtil.getLobbySpawn();

                if (location != null){
                    p.teleport(location);
                }else{
                    p.sendMessage(ChatColor.RED + "There was a problem teleporting you.");
                }
            }else{
                p.sendMessage(ColorUtils.translateColorCodes(Lobbyist.getPlugin().getConfig().getString("messages.no-permission.command")));
            }

        }

        return true;
    }
}
