package me.kodysimpson.lobbyist.utils;

import me.kodysimpson.lobbyist.Lobbyist;
import org.bukkit.Location;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

public class LobbySpawnUtil {

    public static void setLobbySpawn(Location location){

        try{
            ByteArrayOutputStream io = new ByteArrayOutputStream();
            BukkitObjectOutputStream outputStream = new BukkitObjectOutputStream(io);
            outputStream.writeObject(location);
            outputStream.flush();

            String encodedLocation = Base64.getEncoder().encodeToString(io.toByteArray());

            System.out.println(encodedLocation);

            Lobbyist.getPlugin().getConfig().set("lobby-location", encodedLocation);
            Lobbyist.getPlugin().saveConfig();

        }catch (IOException ex){
            System.out.println(ex);
        }

    }

    public static Location getLobbySpawn(){

        try{

            String encodedLocation = Lobbyist.getPlugin().getConfig().getString("lobby-location");

            if (encodedLocation != null){

                ByteArrayInputStream io = new ByteArrayInputStream(Base64.getDecoder().decode(encodedLocation));
                BukkitObjectInputStream inputStream = new BukkitObjectInputStream(io);

                return (Location) inputStream.readObject();
            }else{
                return null;
            }

        }catch (IOException | ClassNotFoundException ex){
            System.out.println(ex);
        }

        return null;
    }

}
