package me.kodysimpson.lobbyist.utils;

import me.kodysimpson.lobbyist.Lobbyist;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Utils {

    public static ItemStack makeItem(Material material, String displayName, List<String> lore) {

        ItemStack item = new ItemStack(material);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(displayName);

        itemMeta.setLore(ColorUtils.translateListColorCodes(lore));
        item.setItemMeta(itemMeta);

        return item;
    }

    public static ItemStack makeItem(Material material, String displayName, String key, int value, List<String> lore) {

        ItemStack item = new ItemStack(material);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(displayName);

        itemMeta.setLore(ColorUtils.translateListColorCodes(lore));

        itemMeta.getPersistentDataContainer().set(new NamespacedKey(Lobbyist.getPlugin(), key), PersistentDataType.INTEGER, value);

        itemMeta.setUnbreakable(true);
        itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);

        item.setItemMeta(itemMeta);

        return item;
    }

    public static ItemStack makeItem(Material material, String displayName, String key, int value) {

        ItemStack item = new ItemStack(material);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(displayName);

        itemMeta.getPersistentDataContainer().set(new NamespacedKey(Lobbyist.getPlugin(), key), PersistentDataType.INTEGER, value);

        itemMeta.setUnbreakable(true);

        item.setItemMeta(itemMeta);

        return item;
    }

    public static List<String> getLobbyWorlds(){

        return Lobbyist.getPlugin().getConfig().getStringList("lobby-worlds");

    }

    public static boolean isPlayerInLobbyWorld(Player p){

        return getLobbyWorlds().contains(p.getWorld().getName());
    }

}
