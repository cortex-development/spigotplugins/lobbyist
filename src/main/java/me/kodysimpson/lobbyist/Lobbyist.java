package me.kodysimpson.lobbyist;

import me.kodysimpson.lobbyist.commands.CommandManager;
import me.kodysimpson.lobbyist.commands.LobbyCommand;
import me.kodysimpson.lobbyist.commands.subcommands.ReloadCommand;
import me.kodysimpson.lobbyist.commands.SetLobbyCommand;
import me.kodysimpson.lobbyist.listeners.*;
import me.kodysimpson.lobbyist.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Objects;

public final class Lobbyist extends JavaPlugin {

    private static Lobbyist plugin;

    @Override
    public void onEnable() {
        // Plugin startup logic
        plugin = this;

        //Setup/Load Config
        getConfig().options().copyDefaults();
        saveDefaultConfig();
        reloadConfig();

        //Our two commands for setting the lobby spawn and teleporting to it
        getCommand("lobby").setExecutor(new LobbyCommand());
        getCommand("setlobby").setExecutor(new SetLobbyCommand());
        getCommand("lobbyist").setExecutor(new CommandManager());

        //Bungeecoord support
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        //Register the listeners
        getServer().getPluginManager().registerEvents(new Listeners(), this);
        getServer().getPluginManager().registerEvents(new MOTDListener(), this);
        getServer().getPluginManager().registerEvents(new JumpListener(), this);
        getServer().getPluginManager().registerEvents(new WeatherListener(), this);
        getServer().getPluginManager().registerEvents(new MenuListener(), this);

        Lobbyist.getPlugin().getConfig().getStringList("always-sunny").stream()
                .filter(world -> Utils.getLobbyWorlds().contains(world))
                .map(Bukkit::getWorld).filter(Objects::nonNull)
                .forEach(world -> world.setStorm(false));

        new BukkitRunnable(){
            @Override
            public void run() {
                Lobbyist.getPlugin().getConfig().getStringList("always-day").stream()
                        .filter(world -> Utils.getLobbyWorlds().contains(world))
                        .map(Bukkit::getWorld).filter(Objects::nonNull)
                        .forEach(world -> world.setTime(6000));
            }
        }.runTaskTimer(this, 0, 1200);


    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public static Lobbyist getPlugin() {
        return plugin;
    }

}
