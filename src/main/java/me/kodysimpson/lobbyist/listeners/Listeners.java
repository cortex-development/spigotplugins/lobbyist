package me.kodysimpson.lobbyist.listeners;

import me.kodysimpson.lobbyist.Lobbyist;
import me.kodysimpson.lobbyist.utils.ColorUtils;
import me.kodysimpson.lobbyist.utils.LobbySpawnUtil;
import me.kodysimpson.lobbyist.utils.Utils;
import net.md_5.bungee.api.ChatMessageType;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

public class Listeners implements Listener {

    private final ItemStack grapplingHook;
    private final ItemStack baconCannon;
    private final ItemStack playerHider;
    private final ItemStack serverSelector;

    private ArrayList<UUID> playersHidingOthers = new ArrayList<>();
    private ArrayList<UUID> baconCooldown = new ArrayList<>();
    private ArrayList<UUID> hiderCooldown = new ArrayList<>();

    public Listeners() {
        this.baconCannon = Utils.makeItem(Material.BOW, ColorUtils.translateColorCodes(Lobbyist.getPlugin().getConfig().getString("hotbar-items.bacon-cannon.name")), "baconBow", 1,
                Lobbyist.getPlugin().getConfig().getStringList("hotbar-items.bacon-cannon.lore"));

        this.grapplingHook = Utils.makeItem(Material.FISHING_ROD, ColorUtils.translateColorCodes(Lobbyist.getPlugin().getConfig().getString("hotbar-items.grappling-hook.name")), "grapplingHook", 1,
                Lobbyist.getPlugin().getConfig().getStringList("hotbar-items.grappling-hook.lore"));

        this.playerHider = Utils.makeItem(Material.LIME_DYE, ColorUtils.translateColorCodes(Lobbyist.getPlugin().getConfig().getString("hotbar-items.player-hider.name")), "player-hider", 1,
                Lobbyist.getPlugin().getConfig().getStringList("hotbar-items.player-hider.lore"));

        this.serverSelector = Utils.makeItem(Material.COMPASS, ColorUtils.translateColorCodes(Lobbyist.getPlugin().getConfig().getString("hotbar-items.server-selector.name")), "server-selector", 1,
                Lobbyist.getPlugin().getConfig().getStringList("hotbar-items.server-selector.lore"));
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {

        Player p = e.getPlayer();

        if (Utils.isPlayerInLobbyWorld(p)){
            //Make sure the player is essentially "godmode"
            p.setHealth(20.0);
            p.setFoodLevel(20);
            p.setAllowFlight(true);

            //If a player joins, get the players that have hidden other players
            //and hide this new player from them.
            playersHidingOthers.stream()
                    .map(Bukkit::getOfflinePlayer)
                    .filter(OfflinePlayer::isOnline)
                    .map(OfflinePlayer::getPlayer).filter(Objects::nonNull)
                    .forEach(player -> player.hidePlayer(Lobbyist.getPlugin(), p));

            //Clear the player's inventory and give them the hotbar items
            setPlayerHotBar(p);

            showJoinTitleAndActionbar(e);
        }

    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e){

        if (Lobbyist.getPlugin().getConfig().getBoolean("keep-inventory-on-death") && Utils.isPlayerInLobbyWorld(e.getEntity())){
            e.setKeepInventory(true);
        }

    }

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent e){

        if (e.getEntity() instanceof Player){
            if (Utils.isPlayerInLobbyWorld((Player) e.getEntity())){
                if (e.getCause() == EntityDamageEvent.DamageCause.VOID){

                    Location location = LobbySpawnUtil.getLobbySpawn();
                    if (location != null){
                        e.getEntity().teleport(location);
                    }

                    e.setCancelled(true);
                }
                if (Lobbyist.getPlugin().getConfig().getBoolean("no-damage-player")){
                    e.setCancelled(true);
                }
            }
        }

    }

    @EventHandler
    public void noHunger(FoodLevelChangeEvent e){

        if (Lobbyist.getPlugin().getConfig().getBoolean("no-hunger-player") && Utils.isPlayerInLobbyWorld((Player) e.getEntity())){
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onBowShoot(EntityShootBowEvent e) {

        if (e.getEntity() instanceof Player) {

            Player p = (Player) e.getEntity();
            ItemMeta meta = e.getBow().getItemMeta();

            if (meta != null && Utils.isPlayerInLobbyWorld(p)) {
                if (meta.getPersistentDataContainer().has(new NamespacedKey(Lobbyist.getPlugin(), "baconBow"), PersistentDataType.INTEGER) && !baconCooldown.contains(e.getEntity().getUniqueId())) {

                    ((Player) e.getEntity()).getInventory().setItem(9, new ItemStack(Material.ARROW));

                    Pig pig = (Pig) e.getEntity().getWorld().spawnEntity(e.getProjectile().getLocation(), EntityType.PIG);
                    pig.setCollidable(false);
                    pig.setInvulnerable(true);
                    pig.setVelocity(e.getProjectile().getVelocity());
                    pig.setGravity(false);
                    pig.getWorld().playSound(pig.getLocation(), Sound.ENTITY_CREEPER_PRIMED, 1.0f, 2.0f);

                    e.setProjectile(pig);

                    baconCooldown.add(e.getEntity().getUniqueId());

                    new BukkitRunnable() {
                        int elapsed = 0;

                        public void run() {

                            if (elapsed >= 100) {
                                pig.remove();
                                baconCooldown.remove(e.getEntity().getUniqueId());
                                this.cancel();
                            }

                            pig.getWorld().spawnParticle(Particle.DRAGON_BREATH, pig.getLocation(), 50, 0D, 0D, 0D);
                            elapsed += 10;
                        }
                    }.runTaskTimer(Lobbyist.getPlugin(), 0, 10);

                }else if (baconCooldown.contains(e.getEntity().getUniqueId())){
                    p.spigot().sendMessage(ChatMessageType.ACTION_BAR, ColorUtils.translateColorCodesToTextComponent(Lobbyist.getPlugin().getConfig().getString("messages.bacon-cannon.cooldown-message")));
                    e.setCancelled(true);
                }
            }

        }

    }

    @EventHandler
    public void onGrapplingHookShoot(PlayerFishEvent e) {

        Player p = e.getPlayer();

        ItemStack item = p.getInventory().getItemInMainHand();

        if (item.getType().equals(Material.FISHING_ROD) && Utils.isPlayerInLobbyWorld(p)) {

            if (item.getItemMeta() != null) {

                if (item.getItemMeta().getPersistentDataContainer().has(new NamespacedKey(Lobbyist.getPlugin(), "grapplingHook"), PersistentDataType.INTEGER)) {

                    if (e.getState() == PlayerFishEvent.State.IN_GROUND) {

                        Location hookLocation = e.getHook().getLocation();
                        Location playerLocation = p.getLocation();

                        //change in location
                        Location change = hookLocation.subtract(playerLocation);

                        p.setVelocity(change.toVector().multiply(0.2));
                    }

                }

            }

        }

    }

    /**
     * Called when the player right clicks an item in their hotbar
     */
    @EventHandler
    public void useHotBarTool(PlayerInteractEvent e) {

        Player p = e.getPlayer();

        ItemStack item = e.getItem();

        if (e.getAction() == Action.RIGHT_CLICK_AIR && item != null && Utils.isPlayerInLobbyWorld(p)) {

            if (item.getItemMeta().getPersistentDataContainer().has(new NamespacedKey(Lobbyist.getPlugin(), "player-hider"), PersistentDataType.INTEGER)) {

                if (!hiderCooldown.contains(p.getUniqueId())){
                    if (playersHidingOthers.contains(e.getPlayer().getUniqueId())) {

                        Bukkit.getServer().getOnlinePlayers()
                                .forEach(player -> p.showPlayer(Lobbyist.getPlugin(), player));

                        p.getInventory().getItemInMainHand().setType(Material.LIME_DYE);

                        p.sendMessage(ColorUtils.translateColorCodes(Lobbyist.getPlugin().getConfig().getString("messages.player-hider.disable")));

                        hiderCooldown.add(p.getUniqueId());

                        new BukkitRunnable(){
                            @Override
                            public void run() {
                                hiderCooldown.remove(p.getUniqueId());
                            }
                        }.runTaskLater(Lobbyist.getPlugin(), 60);

                        playersHidingOthers.remove(e.getPlayer().getUniqueId());
                    } else {
                        hiderCooldown.add(p.getUniqueId());

                        new BukkitRunnable(){
                            @Override
                            public void run() {
                                hiderCooldown.remove(p.getUniqueId());
                            }
                        }.runTaskLater(Lobbyist.getPlugin(), 60);

                        playersHidingOthers.add(e.getPlayer().getUniqueId());

                        p.getInventory().getItemInMainHand().setType(Material.GRAY_DYE);

                        Bukkit.getServer().getOnlinePlayers()
                                .forEach(player -> p.hidePlayer(Lobbyist.getPlugin(), player));

                        p.sendMessage(ColorUtils.translateColorCodes(Lobbyist.getPlugin().getConfig().getString("messages.player-hider.enable")));

                    }
                }else{
                    p.spigot().sendMessage(ChatMessageType.ACTION_BAR, ColorUtils.translateColorCodesToTextComponent(Lobbyist.getPlugin().getConfig().getString("messages.player-hider.cooldown-message")));
                }



            } else if (item.getItemMeta().getPersistentDataContainer().has(new NamespacedKey(Lobbyist.getPlugin(), "server-selector"), PersistentDataType.INTEGER)) {

                Inventory inventory = Bukkit.createInventory(p, Lobbyist.getPlugin().getConfig().getInt("server-selector.slots"), Lobbyist.getPlugin().getConfig().getString("server-selector-menu-name"));

                Lobbyist.getPlugin().getConfig().getConfigurationSection("server-selector.servers").getKeys(false)
                        .forEach(key -> {

                            String serverName = Lobbyist.getPlugin().getConfig().getString("server-selector.servers." + key + ".name");
                            String material = Lobbyist.getPlugin().getConfig().getString("server-selector.servers." + key + ".material");
                            String category = Lobbyist.getPlugin().getConfig().getString("server-selector.servers." + key + ".category");
                            String instruction = Lobbyist.getPlugin().getConfig().getString("server-selector.servers." + key + ".instruction");

                            ItemStack serverItem = new ItemStack(Material.valueOf(material), 1);
                            ItemMeta serverMeta = serverItem.getItemMeta();
                            serverMeta.setDisplayName(ColorUtils.translateColorCodes(serverName));
                            serverMeta.getPersistentDataContainer().set(new NamespacedKey(Lobbyist.getPlugin(), "server-name"), PersistentDataType.STRING, serverName);

                            //lore
                            ArrayList<String> lore = new ArrayList<>();
                            if (category != null){
                                lore.add(category);
                            }
                            lore.add(" ");
                            lore.addAll(Lobbyist.getPlugin().getConfig().getStringList("server-selector.servers." + key + ".lore"));
                            lore.add(" ");
                            if (instruction != null){
                                lore.add(instruction);
                            }

                            if (!lore.isEmpty()) {
                                serverMeta.setLore(lore.stream()
                                        .map(ColorUtils::translateColorCodes)
                                        .collect(Collectors.toList()));
                            }


                            serverItem.setItemMeta(serverMeta);
                            inventory.setItem(Integer.parseInt(key), serverItem);
                        });

                p.openInventory(inventory);

            }


        }


    }

    /**
     * Prevent the player from dropping items unless specified
     * in the config otherwise
     */
    @EventHandler
    public static void onDropItem(PlayerDropItemEvent e){

        if (Lobbyist.getPlugin().getConfig().getBoolean("no-item-drop") && Utils.isPlayerInLobbyWorld(e.getPlayer())){
            e.setCancelled(true);
        }

    }

    /**
     * Prevent the player from picking up items unless specified
     * in the config otherwise
     */
    @EventHandler
    public static void onItemPickup(EntityPickupItemEvent e){

        if (Lobbyist.getPlugin().getConfig().getBoolean("no-item-pickup") && e.getEntity() instanceof Player && Utils.isPlayerInLobbyWorld((Player) e.getEntity())){
            e.setCancelled(true);
        }

    }

    private void showJoinTitleAndActionbar(PlayerJoinEvent e) {
        String title = Lobbyist.getPlugin().getConfig().getString("join-title");
        String subtitle = Lobbyist.getPlugin().getConfig().getString("join-subtitle");
        String actionbar = Lobbyist.getPlugin().getConfig().getString("join-actionbar");

        int fadeIn = Lobbyist.getPlugin().getConfig().getInt("title-duration.fadein");
        int stay = Lobbyist.getPlugin().getConfig().getInt("title-duration.stay");
        int fadeOut = Lobbyist.getPlugin().getConfig().getInt("title-duration.fadeout");

        if (title != null && subtitle != null) {
            e.getPlayer().sendTitle(ColorUtils.translateColorCodes(title), ColorUtils.translateColorCodes(subtitle), fadeIn, stay, fadeOut);
        }

        if (actionbar != null) {
            e.getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR, ColorUtils.translateColorCodesToTextComponent(actionbar));
        }else{
            System.out.println("There was an issue translating the join-actionbar in the config.yml");
        }
    }

    private void setPlayerHotBar(Player p) {
        p.getInventory().clear();
        p.getInventory().setItem(Lobbyist.getPlugin().getConfig().getInt("hotbar-items.server-selector.slot") - 1, serverSelector);
        p.getInventory().setItem(Lobbyist.getPlugin().getConfig().getInt("hotbar-items.bacon-cannon.slot") - 1, baconCannon);
        p.getInventory().setItem(Lobbyist.getPlugin().getConfig().getInt("hotbar-items.grappling-hook.slot") - 1, grapplingHook);
        p.getInventory().setItem(Lobbyist.getPlugin().getConfig().getInt("hotbar-items.player-hider.slot") - 1, playerHider);
        p.getInventory().setItem(9, new ItemStack(Material.ARROW));
    }

}
