package me.kodysimpson.lobbyist.listeners;

import me.kodysimpson.lobbyist.Lobbyist;
import me.kodysimpson.lobbyist.utils.ColorUtils;
import me.kodysimpson.lobbyist.utils.Utils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

public class MOTDListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e){

        Player p = e.getPlayer();

        if (Utils.isPlayerInLobbyWorld(p)){
            new BukkitRunnable(){
                @Override
                public void run() {
                    //MOTD
                    ArrayList<String> motd = (ArrayList<String>) Lobbyist.getPlugin().getConfig().getStringList("motd");
                    if (!motd.isEmpty()){
                        motd.forEach(message -> p.sendMessage(ColorUtils.translateColorCodes(message)));
                    }
                }
            }.runTaskLater(Lobbyist.getPlugin(), 20);
        }

    }

}
