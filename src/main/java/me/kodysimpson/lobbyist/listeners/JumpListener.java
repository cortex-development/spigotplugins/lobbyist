package me.kodysimpson.lobbyist.listeners;

import me.kodysimpson.lobbyist.Lobbyist;
import me.kodysimpson.lobbyist.utils.Utils;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.UUID;

public class JumpListener implements Listener {

    private ArrayList<UUID> jumpingPlayers = new ArrayList<>();

    @EventHandler
    public void onJump(PlayerToggleFlightEvent event) {

        Player player = event.getPlayer();

        if (Utils.isPlayerInLobbyWorld(player)){
            if (!jumpingPlayers.contains(player.getUniqueId())){
                if(event.isFlying() && event.getPlayer().getGameMode() != GameMode.CREATIVE) {

                    player.setFlying(false);
                    Vector jump = player.getLocation().getDirection().multiply(4).setY(2);
                    player.setVelocity(player.getVelocity().add(jump));

                    jumpingPlayers.add(player.getUniqueId());

                    new BukkitRunnable(){
                        @Override
                        public void run() {
                            jumpingPlayers.remove(player.getUniqueId());
                        }
                    }.runTaskLater(Lobbyist.getPlugin(), 100);


                    event.setCancelled(true);
                }
            }else{

                player.setFlying(false);
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onGamemodeSwitch(PlayerGameModeChangeEvent e){

        if (e.getNewGameMode().equals(GameMode.SURVIVAL) && Utils.isPlayerInLobbyWorld(e.getPlayer())){
            e.getPlayer().setAllowFlight(true);
        }

    }

    @EventHandler
    public void onJumpPadStep(PlayerMoveEvent e) {

        Player p = e.getPlayer();

        if (Utils.isPlayerInLobbyWorld(p)){
            Block topBlock = p.getLocation().getBlock();
            Block underBlock = p.getLocation().subtract(0, 1, 0).getBlock();

            Material topMaterial = Material.valueOf(Lobbyist.getPlugin().getConfig().getString("top-block"));
            Material underMaterial = Material.valueOf(Lobbyist.getPlugin().getConfig().getString("under-block"));

            if ((Lobbyist.getPlugin().getConfig().getBoolean("top-block-enabled") ? (topBlock.getType() == topMaterial && underBlock.getType() == underMaterial) : underBlock.getType() == underMaterial)) {
                Vector playerVelocity = p.getLocation().getDirection();
                playerVelocity.multiply(Lobbyist.getPlugin().getConfig().getDouble("velocity-multiplier")).setY(2);
                p.setVelocity(playerVelocity);

                p.getWorld().spawnParticle(Particle.valueOf(Lobbyist.getPlugin().getConfig().getString("particle-effect")), p.getLocation(), 20);
                p.getWorld().playSound(p.getLocation(), Sound.valueOf(Lobbyist.getPlugin().getConfig().getString("sound-effect")), 1.0f, 2.0f);
            }
        }

    }

}
