package me.kodysimpson.lobbyist.listeners;

import me.kodysimpson.lobbyist.Lobbyist;
import me.kodysimpson.lobbyist.utils.Utils;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.persistence.PersistentDataType;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class MenuListener implements Listener {

    @EventHandler
    public void onMenuClick(InventoryClickEvent e) {

        Player p = (Player) e.getWhoClicked();

        if (e.getView().getTitle().equalsIgnoreCase(Lobbyist.getPlugin().getConfig().getString("server-selector-menu-name")) && Utils.isPlayerInLobbyWorld(p)) {

            ItemStack item = e.getCurrentItem();

            if (item.getItemMeta().getPersistentDataContainer().has(new NamespacedKey(Lobbyist.getPlugin(), "server-name"), PersistentDataType.STRING)){

                p.closeInventory();

                e.setCancelled(true);

                teleportPlayerToServer(p, item.getItemMeta().getPersistentDataContainer().get(new NamespacedKey(Lobbyist.getPlugin(), "server-name"), PersistentDataType.STRING));
            }

        }else if (e.getClickedInventory() instanceof PlayerInventory && Utils.isPlayerInLobbyWorld(p)){
            e.setCancelled(true);
        }

    }

    public static void teleportPlayerToServer(final Player player, final String server) {

        try (
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(baos)
        ) {

            dos.writeUTF("Connect");
            dos.writeUTF(server);
            player.sendPluginMessage(Lobbyist.getPlugin(), "BungeeCord", baos.toByteArray());
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
}
