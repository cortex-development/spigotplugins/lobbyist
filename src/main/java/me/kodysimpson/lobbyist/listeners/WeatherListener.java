package me.kodysimpson.lobbyist.listeners;

import me.kodysimpson.lobbyist.Lobbyist;
import me.kodysimpson.lobbyist.utils.Utils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

import java.util.ArrayList;

public class WeatherListener implements Listener {

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent e){

        if (Utils.getLobbyWorlds().contains(e.getWorld().getName())){
            ArrayList<String> worlds = (ArrayList<String>) Lobbyist.getPlugin().getConfig().getStringList("always-sunny");
            if (e.toWeatherState() && worlds.contains(e.getWorld().getName())){
                e.setCancelled(true);
            }
        }

    }

}
